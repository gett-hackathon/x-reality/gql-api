package google

import (
	"gitlab.com/gett-hackathon/x-reality/gql-api/pkg/multiauth"
)

// UserInfo represents the userinfos returned by the google userinfo endpoint.
type UserInfo struct {
	Subject       string `json:"sub"`
	GivenName     string `json:"given_name"`
	FamilyName    string `json:"family_name"`
	PictureURL    string `json:"picture"`
	Email         string `json:"email"`
	EmailVerified bool   `json:"email_verified"`
}

// Universal returns a multiauth.UserInfo containing all in the given UserInfo present information.
func (u UserInfo) Universal() multiauth.UserInfo {
	return multiauth.UserInfo{
		Subject:       u.Subject,
		EmailVerified: u.EmailVerified,
		Email:         u.Email,
		FamilyName:    u.FamilyName,
		GivenName:     u.GivenName,
		PictureURL:    u.PictureURL,
	}
}
