package google

import (
	"gitlab.com/gett-hackathon/x-reality/gql-api/pkg/multiauth"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
)

// client is a google based Client implementation,
type client struct {
	*multiauth.ClientBase
}

// New creates a new google extauth client.
func New(id, secret, redirectURL string) multiauth.Client {
	return &client{
		ClientBase: multiauth.NewClientBase(&oauth2.Config{
			ClientID:     id,
			ClientSecret: secret,
			Endpoint:     google.Endpoint,
			RedirectURL:  redirectURL,
			Scopes:       []string{"profile", "https://www.googleapis.com/auth/userinfo.email"},
		}),
	}
}

// ID returns the clients ID (google in this case)
func (c *client) ID() string {
	return "google"
}

// Authenticate authenticates a user using the given code and returns his userinfo.
func (c *client) Authenticate(code string) (multiauth.UserInfo, error) {
	info := UserInfo{}
	if err := c.FetchUserInfo("https://www.googleapis.com/oauth2/v3/userinfo", code, &info); err != nil {
		return multiauth.UserInfo{}, err
	}

	return info.Universal(), nil
}
