package multiauth

import "time"

// UserInfo reprents an exauth accounts user info.
type UserInfo struct {
	Subject       string    `json:"subject"`
	PictureURL    string    `json:"picture_url"`
	GivenName     string    `json:"given_name"`
	FamilyName    string    `json:"family_name"`
	Email         string    `json:"email"`
	EmailVerified bool      `json:"email_verified"`
	BirthDay      time.Time `json:"birthday"`
}
