package bmath

// ClampInt clams the given value between the given min and max values
func ClampInt(val, min, max int) int {
	if val < min || max < min {
		return min
	}

	if val > max {
		return max
	}

	return val
}

// MinInt returns the in with the smallest value of the 2 given ints
func MinInt(val1, val2 int) int {
	if val1 < val2 {
		return val1
	}

	return val2
}

// MaxInt returns the int with highest value of the 2 given ints
func MaxInt(val1, val2 int) int {
	if val1 > val2 {
		return val1
	}

	return val2
}
