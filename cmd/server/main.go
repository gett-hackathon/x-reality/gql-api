// nolint: errcheck, gosec
package main

import (
	"fmt"
	"log"
	"strconv"

	"github.com/couchbase/gocb"
	"github.com/go-chi/chi"
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/account"
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/app"
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/config"
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/couchbase"
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/extauth"
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/graphql/graph"
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/graphql/reqestcontext"
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/http"
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/http/handler"
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/logging"
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/passwordauth"
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/session"
	"gitlab.com/gett-hackathon/x-reality/gql-api/pkg/multiauth"
	"gitlab.com/gett-hackathon/x-reality/gql-api/pkg/multiauth/providers/google"
	"go.uber.org/dig"
)

func main() {
	fmt.Println("ok")

	container := dig.New()

	container.Provide(config.NewService)
	container.Provide(func(c config.Service) config.Config {
		return c.Config()
	})

	container.Provide(func(c config.Config) (logging.Logger, error) {
		lvl, err := logging.LevelFromString(c.LoggingLevel)
		if err != nil {
			log.Fatal("msg", "invalid logging level", "level", c.LoggingLevel)
		}

		return logging.New(lvl)
	})

	container.Provide(func(c config.Config) (*gocb.Bucket, error) {
		cluster, err := gocb.Connect(c.Couchbase.URL)
		if err != nil {
			return nil, err
		}

		if err := cluster.Authenticate(gocb.PasswordAuthenticator{
			Username: c.Couchbase.User,
			Password: c.Couchbase.Password,
		}); err != nil {
			return nil, err
		}

		return cluster.OpenBucket(c.Couchbase.Bucket, "")
	})

	container.Provide(func(c config.Config) *multiauth.Manager {
		callbackURL := c.URL + handler.ExtauthCallbackRoute

		return multiauth.NewManager(
			google.New(c.Extauth.Google.ClientID, c.Extauth.Google.Secret, callbackURL),
		)
	})

	container.Provide(couchbase.NewExtauthRepository)
	container.Provide(couchbase.NewPasswordAuthRepository)
	container.Provide(couchbase.NewAppRepository)

	container.Provide(app.NewService)

	container.Provide(func(bucket *gocb.Bucket) account.Repository {
		return couchbase.NewAccountRepository(bucket, 1, 0)
	})

	container.Provide(func(accountRepo account.Repository, sessionSrv session.Service, c config.Config, log logging.Logger) account.Service {
		return account.NewLoggingService(account.NewService(accountRepo, sessionSrv, c), log)
	})

	container.Provide(func(repo passwordauth.Repository, accountService account.Service, log logging.Logger) passwordauth.Service {
		return passwordauth.NewLoggingService(passwordauth.NewService(repo, accountService), log)
	})

	container.Provide(func(repo extauth.Repository, manager *multiauth.Manager, accountService account.Service, log logging.Logger) extauth.Service {
		return extauth.NewLoggingService(extauth.NewService(repo, manager, accountService), log)
	})

	container.Provide(func(c config.Config, log logging.Logger) (session.Service, error) {
		s, err := session.NewService(c)
		if err != nil {
			return nil, err
		}

		return session.NewLoggingService(s, log), nil
	})

	container.Provide(graph.NewAppResolver)
	container.Provide(rctx.NewInjector)
	container.Provide(graph.NewQueryResolver)
	container.Provide(graph.NewMutationResolver)
	container.Provide(graph.NewResolverRoot)

	container.Provide(handler.NewGraphQL)
	container.Provide(handler.NewAuthentication)

	if err := container.Invoke(func(c config.Config, gqlHandler *handler.GraphQL, authHandler *handler.Authentication, log logging.Logger) {
		fmt.Println("ok")

		r := chi.NewRouter()
		r.Use(http.CORSAllowAll())

		fmt.Println("ok")

		gqlHandler.RegisterRoutes(r)
		authHandler.RegisterRoutes(r)

		fmt.Println("ok")

		s := http.NewServer(":"+strconv.Itoa(c.Port), r)

		fmt.Println("ok")

		log.Infow("Starting server", "port", c.Port)
		fmt.Println("ok")

		if err := s.ListenAndServe(); err != nil {
			log.Fatalw("Unable to start server", "err", err)
		}

	}); err != nil {
		panic(err)
	}
}
