
#### 0-100 General errors
Code | Meaning
------|--------
0    | Unknown error 
11   | Not found
12   | Already exists
20   | Permission denied
30   | Validation

#### 100-200 Login/Registration Error
Code | Meaning
-----|--------
101  | Email is already taken
102  | Nickname is already taken
103  | Password it too short
104  | Wrong password
110  | (Extauth only) Extauth account is already registered
111  | (Extauth only) Obtained account info doesn't contain all the for the registration necessary data
121  | The accounts password isn't set (Most likely because he signed up using extauth)
131  | Email is already verified
141  | Session expired
142  | Unexpected signing methods
143  | Invalid token
