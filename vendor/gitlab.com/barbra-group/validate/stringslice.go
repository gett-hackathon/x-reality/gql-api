package validate

import (
	"errors"
	"fmt"
)

type allStringSliceRules struct {
	rules []StringRule
}

// Validates checks whether the sting is compliant with all of the allStringSliceRules containing rules. If not an error will be returned.
func (r allStringSliceRules) Validate(s []string) error {
	for _, val := range s {
		for _, rule := range r.rules {
			if err := rule.Validate(val); err != nil {
				return errors.New("all " + err.Error())
			}
		}
	}

	return nil
}

// AllStringRules creates a new allStringSliceRules using the given rules.
func AllStringRules(rules ...StringRule) StringSliceRule {
	return &allStringSliceRules{rules}
}

// HasStringSliceSize returns a StringSliceRule to verify that the amount of items inside the string slice is in the specified range.
func HasStringSliceSize(min, max int) StringSliceRule {
	return NewStringSliceRule(func(s []string) bool {
		return len(s) >= min && len(s) <= max
	}, fmt.Sprintf("should contain between %d and %d items", min, max))
}
