package validate

import "errors"

// StringValidator is a function used to validate strings.
type StringValidator func(s string) bool

// StringPtr validates a string pointer using the given rules.
func StringPtr(val *string, fieldName string, nilable, allowDefault bool, rules ...StringRule) error {
	if !nilable && val == nil {
		return &Error{fieldName, ErrNotNilable.Error()}
	}

	if val == nil {
		return nil
	}

	return String(*val, fieldName, allowDefault, rules...)
}

// String validates a string using the given rules.
func String(val, fieldName string, optional bool, rules ...StringRule) error {
	if val == "" && !optional {
		return &Error{fieldName, ErrRequired.Error()}
	}

	if val == "" {
		return nil
	}

	for _, rule := range rules {
		if err := rule.Validate(val); err != nil {
			return &Error{fieldName, err.Error()}
		}
	}

	return nil
}

type StringRule interface {
	Validate(s string) error
}

// NewStringRule creates a new string rule using the given validator and error message.
func NewStringRule(validator StringValidator, msg string) StringRule {
	return &stringRule{validator, msg}
}

type stringRule struct {
	validator StringValidator
	msg       string
}

// Validates checks whether the sting is compliant to the string rule. If not an error will be returned.
func (r stringRule) Validate(s string) error {
	if !r.validator(s) {
		return errors.New(r.msg)
	}

	return nil
}
