package validate

import "fmt"

// Error represents an validation error.
type Error struct {
	fieldName string
	msg       string
}

// Error return the Error error msg.
func (e Error) Error() string {
	return fmt.Sprintf("%v: %v", e.fieldName, e.msg)
}

// Errors represents multiple validation errors.
type FieldErrors struct {
	name   string
	errors []error
}

// Error return the Errors error msg.
func (e FieldErrors) Error() string {
	msg := e.name + " is not valid: "
	for i, e := range e.errors {
		if i == 0 {
			msg += e.Error()
			continue
		}

		msg += ", " + e.Error()
	}
	return msg
}
