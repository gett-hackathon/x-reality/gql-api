module gitlab.com/gett-hackathon/x-reality/gql-api

require (
	cloud.google.com/go v0.33.1 // indirect
	github.com/99designs/gqlgen v0.7.1
	github.com/agnivade/levenshtein v1.0.1 // indirect
	github.com/andreyvit/diff v0.0.0-20170406064948-c7f18ee00883 // indirect
	github.com/couchbase/gocb v1.5.0
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/fortytw2/leaktest v1.3.0 // indirect
	github.com/go-chi/chi v3.3.3+incompatible
	github.com/golang/snappy v0.0.0-20180518054509-2e65f85255db // indirect
	github.com/google/uuid v1.0.0 // indirect
	github.com/gorilla/websocket v1.4.0 // indirect
	github.com/hashicorp/golang-lru v0.5.0 // indirect
	github.com/kelseyhightower/envconfig v1.3.0
	github.com/kr/pretty v0.1.0 // indirect
	github.com/mailru/easyjson v0.0.0-20180823135443-60711f1a8329 // indirect
	github.com/olivere/elastic v6.2.13+incompatible
	github.com/opentracing/opentracing-go v1.0.2 // indirect
	github.com/pkg/errors v0.8.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/satori/go.uuid v1.2.0
	github.com/sendgrid/rest v2.4.1+incompatible // indirect
	github.com/sendgrid/sendgrid-go v3.4.1+incompatible
	github.com/sergi/go-diff v1.0.0 // indirect
	github.com/stretchr/testify v1.2.2 // indirect
	github.com/vektah/gqlparser v0.0.0-20181113085259-6f09700b13a3
	gitlab.com/barbra-group/validate v0.0.0-20181106175744-b8d718631860
	go.uber.org/atomic v1.3.2 // indirect
	go.uber.org/dig v1.6.0
	go.uber.org/multierr v1.1.0 // indirect
	go.uber.org/zap v1.9.1
	golang.org/x/crypto v0.0.0-20181112202954-3d3f9f413869
	golang.org/x/net v0.0.0-20181114220301-adae6a3d119a // indirect
	golang.org/x/oauth2 v0.0.0-20181106182150-f42d05182288
	golang.org/x/sync v0.0.0-20181108010431-42b317875d0f // indirect
	google.golang.org/appengine v1.3.0 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gopkg.in/couchbase/gocbcore.v7 v7.1.8 // indirect
	gopkg.in/couchbaselabs/gocbconnstr.v1 v1.0.2 // indirect
	gopkg.in/couchbaselabs/gojcbmock.v1 v1.0.3 // indirect
	gopkg.in/couchbaselabs/jsonx.v1 v1.0.0 // indirect
	gopkg.in/yaml.v2 v2.2.1 // indirect
)
