package handler

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/go-chi/chi"
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/config"
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/errors"
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/extauth"
	internalHttp "gitlab.com/gett-hackathon/x-reality/gql-api/internal/http"
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/logging"
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/passwordauth"
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/session"
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/uuid"
)

const (
	// LoginRoute is the http route used to handle password based login request.
	LoginRoute = "/login"

	// RegistrationRoute is the http route used to handle password based registration request.
	RegistrationRoute = "/register"

	// ExtauthLoginRoute is the http route used to handle login request based on external auth providers (google, facebook etc.).
	ExtauthLoginRoute = "/extauth/{provider}/login"

	// ExtauthRegistrationRoute is the http route used to handle registration request based on external auth providers (google, facebook etc.).
	ExtauthRegistrationRoute = "/extauth/{provider}/register"

	// ExtauthCallbackRoute is the http route used to handle external auth provider callbacks.
	ExtauthCallbackRoute = "/extauth/callback"

	// LoginSessionName is the cookie name of a login session
	LoginSessionName = "login_session"

	// SessionName is the cookie name of a session
	SessionName = "session"
)

// Authentication is the authentication http handler
type Authentication struct {
	pwAuthSrv          passwordauth.Service
	extauthSrv         extauth.Service
	sessionSrv         session.Service
	redirectErrHandler *internalHttp.ErrorHandler
	statusErrHandler   *internalHttp.ErrorHandler
	domain             string
	redirectURL        string
}

// NewAuthentication creates a new http authentication handler.
func NewAuthentication(pwAuthSrv passwordauth.Service, extauthSrv extauth.Service, sessionSrv session.Service, c config.Config, log logging.Logger) *Authentication {
	return &Authentication{
		sessionSrv:         sessionSrv,
		extauthSrv:         extauthSrv,
		pwAuthSrv:          pwAuthSrv,
		domain:             c.Domain,
		redirectURL:        c.LoginRedirectURL,
		redirectErrHandler: internalHttp.NewErrorHandler(log, internalHttp.NewRedirectErrorPresenter(c.ErrorRedirectURL)),
		statusErrHandler:   internalHttp.NewErrorHandler(log, internalHttp.NewStatusCodeErrorPresenter()),
	}
}

// RegisterRoutes registers the handlers routes on the given router
func (h *Authentication) RegisterRoutes(r chi.Router) {
	r.Get(LoginRoute, h.Login)
	r.Post(RegistrationRoute, h.Register)
	r.Get(ExtauthLoginRoute, h.ExtauthLogin)
	r.Get(ExtauthRegistrationRoute, h.ExtauthRegister)
	r.Get(ExtauthCallbackRoute, h.ExtauthCallback)
}

// Login handles login requests using a "traditional" password
func (h *Authentication) Login(w http.ResponseWriter, r *http.Request) {
	p := &passwordauth.LoginParams{}
	if err := json.NewDecoder(r.Body).Decode(p); err != nil {
		h.statusErrHandler.Handle(w, r, err)
		return
	}

	sub, err := h.pwAuthSrv.Login(*p)
	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	h.setAndRedirect(w, r, session.New(sub))
}

// Register handles registration requests using a "traditional" password
func (h *Authentication) Register(w http.ResponseWriter, r *http.Request) {
	p := &passwordauth.RegisterParams{}
	if err := json.NewDecoder(r.Body).Decode(p); err != nil {
		h.statusErrHandler.Handle(w, r, err)
		return
	}

	sub, err := h.pwAuthSrv.Register(*p)
	if err != nil {
		return
	}

	h.setAndRedirect(w, r, session.New(sub))
}

// ExtauthLogin handles exauth login requests.
func (h *Authentication) ExtauthLogin(w http.ResponseWriter, r *http.Request) {
	h.extAuthRedirect(false, w, r)
}

// ExtauthRegister handles exauth registration requests.
func (h *Authentication) ExtauthRegister(w http.ResponseWriter, r *http.Request) {
	h.extAuthRedirect(true, w, r)
}

// ExtauthCallback handles oauth callbacks from known extauth providers.
func (h *Authentication) ExtauthCallback(w http.ResponseWriter, r *http.Request) {
	errMsg := r.URL.Query().Get("error")
	if errMsg != "" {
		h.redirectErrHandler.Handle(w, r, errors.New(errors.Unknown, errMsg))
		return
	}

	c, err := r.Cookie(LoginSessionName)
	if err != nil {
		h.redirectErrHandler.Handle(w, r, err)
		return
	}

	s := &session.Login{}
	if err = h.sessionSrv.Parse(s, c.Value); err != nil {
		h.redirectErrHandler.Handle(w, r, err)
		return
	}

	var sub uuid.ID
	if s.Register {
		sub, err = h.extauthSrv.Register(s.Provider, r.URL.Query().Get("code"))
	} else {
		sub, err = h.extauthSrv.Login(s.Provider, r.URL.Query().Get("code"))
	}

	if err != nil {
		h.redirectErrHandler.Handle(w, r, err)
		return
	}

	h.setAndRedirect(w, r, session.New(sub))
}

// extAuthRedirect creates and saves a new login session and redirects to the in the request vars specified exauth provider
func (h *Authentication) extAuthRedirect(register bool, w http.ResponseWriter, r *http.Request) {
	provider := chi.URLParam(r, "provider")
	if len(provider) < 1 {
		h.redirectErrHandler.Handle(w, r, ErrIncompleteRequestData)
		return
	}

	state, err := h.extauthSrv.RandomState()
	if err != nil {
		h.redirectErrHandler.Handle(w, r, err)
		return
	}

	s := session.NewLogin(state, provider, register)
	token, err := h.sessionSrv.Token(s)
	if err != nil {
		h.redirectErrHandler.Handle(w, r, err)
		return
	}

	http.SetCookie(w, &http.Cookie{
		Expires: time.Unix(s.ExpiresAt, 0),
		Value:   token,
		// Secure:   true,
		HttpOnly: true,
		Name:     LoginSessionName,
		Path:     ExtauthCallbackRoute,
	})

	authURL, err := h.extauthSrv.AuthCodeURL(provider, state)
	if err != nil {
		h.redirectErrHandler.Handle(w, r, err)
		return
	}

	http.Redirect(w, r, authURL, http.StatusFound)
}

// setAndRedirect saves the given session into a cookie and redirects to the login successful redirect url.
func (h *Authentication) setAndRedirect(w http.ResponseWriter, r *http.Request, s session.Session) {
	token, err := h.sessionSrv.Token(s)
	if err != nil {
		h.redirectErrHandler.Handle(w, r, err)
		return
	}

	fmt.Println(token)

	http.SetCookie(w, &http.Cookie{
		Value:   token,
		Name:    SessionName,
		Domain:  h.domain,
		Secure:  true,
		Expires: time.Unix(s.ExpiresAt, 0),
		Path:    "/",
	})

	http.Redirect(w, r, h.redirectURL, http.StatusFound)
}
