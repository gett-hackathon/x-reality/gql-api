package http

import (
	"net/http"
)

// CORSAllowAll creates a new http middleware which sets the CORS headers to allow all requests
func CORSAllowAll() func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if origin := r.Header.Get("Origin"); origin != "" {
				writeHeaders(w, origin)
			}

			next.ServeHTTP(w, r)
		})
	}
}

// CORSWhiteList creates a new http middleware which sets the CORS headers to allow the request if the whitelist contains the request origin
func CORSWhiteList(allowedOrigins []string) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		oMap := make(map[string]bool)
		for _, o := range allowedOrigins {
			oMap[o] = true
		}

		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			origin := r.Header.Get("Origin")

			if ok, exists := oMap[origin]; origin != "" && ok && exists {
				writeHeaders(w, origin)
			}

			next.ServeHTTP(w, r)
		})
	}
}

// writeHeaders writes the cors headers to allow the given request
func writeHeaders(w http.ResponseWriter, origin string) {
	w.Header().Set("Access-Control-Allow-Origin", origin)
	w.Header().Set("Access-Control-Allow-Methods", "POST")
	w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, Authorization")
	w.Header().Set("Access-Control-Allow-Credentials", "true")
}
