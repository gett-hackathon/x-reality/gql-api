package extauth

import "gitlab.com/gett-hackathon/x-reality/gql-api/internal/uuid"

// Account is used to link a oauth account to an account
type Account struct {
	// The provider of the oauth account (e.g. Google, Facebook)
	Provider string `json:"provider"`

	// The id of the oauth accounts subject (provided by the oauth provider)
	Subject string `json:"subject"`

	// The id of the oAuth's account owner
	OwnerID uuid.ID `json:"owner_id"`
}

// NewAccount creates a new oauth account using the given parameters
func NewAccount(provider, subject string, ownerID uuid.ID) (*Account, error) {
	return &Account{
		Provider: provider,
		Subject:  subject,
		OwnerID:  ownerID,
	}, nil
}
