package extauth

import (
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/logging"
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/uuid"
)

// loggingService provides a logging wrapper around Service.
type loggingService struct {
	next Service
	log  logging.Logger
}

// NewLoggingService creates a new logging service.
func NewLoggingService(next Service, log logging.Logger) Service {
	return &loggingService{
		log:  log.WithFields("service", "Extauth"),
		next: next,
	}
}

// Register provides a logging wrapper around service.Register.
func (s *loggingService) Register(provider, code string) (id uuid.ID, err error) {
	defer s.log.Debugw("called method Register", "provider", provider, "id", id, "error", err)
	return s.next.Register(provider, code)
}

// Register provides a logging wrapper around service.Register.
func (s *loggingService) Login(provider, code string) (id uuid.ID, err error) {
	defer s.log.Debugw("called method Login", "provider", provider, "id", id, "error", err)
	return s.next.Login(provider, code)
}

// AuthCodeURL provides a logging wrapper around service.AuthCodeURL.
func (s *loggingService) AuthCodeURL(provider, state string) (url string, err error) {
	defer s.log.Debugw("called method AuthCodeURL", "provider", provider, "state", state, "url", url, "error", err)
	return s.next.AuthCodeURL(provider, state)
}

// AuthCodeURL provides a logging wrapper around service.AuthCodeURL.
func (s *loggingService) RandomState() (state string, err error) {
	defer s.log.Debugw("called method RandomState", "state", state, "error", err)
	return s.next.RandomState()
}
