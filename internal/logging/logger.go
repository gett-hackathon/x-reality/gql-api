package logging

import (
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

// Logger provides all logging related functionality
type Logger interface {
	// WithFields returns a new logger with the given fields added to the current loggers fields.
	WithFields(fields ...interface{}) Logger

	// Debugw logs a message with some additional context. The variadic key-value
	// pairs are treated as they are in With.
	//
	// When debug-level logging is disabled, this is much faster than
	//  s.With(keysAndValues).Debug(msg)
	Debugw(msg string, keysAndValues ...interface{})

	// Infow logs a message with some additional context. The variadic key-value
	// pairs are treated as they are in With.
	Infow(msg string, keysAndValues ...interface{})

	// Warnw logs a message with some additional context. The variadic key-value
	// pairs are treated as they are in With.
	Warnw(msg string, keysAndValues ...interface{})

	// Errorw logs a message with some additional context. The variadic key-value
	// pairs are treated as they are in With.
	Errorw(msg string, keysAndValues ...interface{})

	// Fatalw logs a message with some additional context, then calls os.Exit. The
	// variadic key-value pairs are treated as they are in With.
	Fatalw(msg string, keysAndValues ...interface{})

	// Panicw logs a message with some additional context, then panics. The
	// variadic key-value pairs are treated as they are in With.
	Panicw(msg string, keysAndValues ...interface{})

	// Sync flushes any buffered log entries.
	Sync() error
}

type logger struct {
	*zap.SugaredLogger
}

// New creates a new logger with the given logging level.
func New(level Level) (Logger, error) {
	cfg := zap.NewProductionConfig()
	cfg.Level = zap.NewAtomicLevelAt(zapcore.Level(level))
	cfg.OutputPaths = []string{"stdout"}

	l, err := cfg.Build()
	if err != nil {
		return nil, err
	}

	return &logger{l.Sugar()}, nil
}

// WithFields returns a new logger with the given fields added to the current loggers fields.
func (l *logger) WithFields(fields ...interface{}) Logger {
	return &logger{
		l.With(fields...),
	}
}
