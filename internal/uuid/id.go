package uuid

import (
	"strconv"

	"github.com/satori/go.uuid"
)

// ID represents a UUIDv4
type ID string

// New creates a new, unique UUIDv4
func New() ID {
	return ID(uuid.Must(uuid.NewV4(), nil).String())
}

// FromString parses a string to a UUIDv4
func FromString(s string) (ID, error) {
	return ID(s), nil
}

// String returns the string representation of id
func (i ID) String() string {
	return string(i)
}

// MarshalJSON is the custom ID marshaller
func (i *ID) MarshalJSON() ([]byte, error) {
	return []byte(strconv.Quote(i.String())), nil
}

// UnmarshalJSON is the custom ID unmarshaller
func (i *ID) UnmarshalJSON(data []byte) error {
	s, err := strconv.Unquote(string(data))
	if err != nil {
		return err
	}

	id, err := FromString(s)
	if err != nil {
		return err
	}

	*i = id
	return nil
}
