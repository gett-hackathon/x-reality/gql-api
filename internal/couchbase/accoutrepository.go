package couchbase

import (
	"strings"
	"time"

	"github.com/couchbase/gocb"
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/account"
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/uuid"
)

// NewAccountRepository creates a new dataloaded couchbase based account.Repository.
func NewAccountRepository(bucket *gocb.Bucket, maxBatchSize int, maxWait time.Duration) account.Repository {
	return &accountRepository{
		bucket: bucket,
		loader: NewAccountLoader(bucket, maxBatchSize, maxWait),
	}
}

type accountRepository struct {
	bucket *gocb.Bucket
	loader *AccountLoader
}

func (r *accountRepository) Get(id uuid.ID) (*account.Account, error) {
	acc, err := r.loader.Load(SerializeKey(AccountType, id.String()))
	return acc, r.toInternalErr(err)
}

func (r *accountRepository) GetByNickname(nickname string) (*account.Account, error) {
	l := &LookupKey{}
	if _, err := r.bucket.Get(SerializeKey(NicknameLookupType, nickname), l); err != nil {
		return nil, r.toInternalErr(err)
	}

	return r.Get(l.Key)
}

func (r *accountRepository) GetByEmail(email string) (*account.Account, error) {
	l := &LookupKey{}
	if _, err := r.bucket.Get(SerializeKey(EmailLookupType, email), &l); err != nil {
		return nil, r.toInternalErr(err)
	}

	return r.Get(l.Key)
}

// TODO: Use transactional logic. There could be a very very unlikely case that 1 user registers in at the same time and the email will be
// taken before the account was created thus making the account inaccessible (Well still that wouldn't be a problem but we would have one
// "dead" account in the database) Keys should be done using transactions (will do it later)
func (r *accountRepository) Insert(acc *account.Account) error {
	blkop := []gocb.BulkOp{
		&gocb.InsertOp{Key: SerializeKey(AccountType, acc.ID.String()), Value: NewDocument(AccountType, 1, acc)},
	}

	if acc.Email != "" {
		blkop = append(blkop, &gocb.InsertOp{Key: SerializeKey(EmailLookupType, strings.ToLower(acc.Email)), Value: &LookupKey{acc.ID}})
	}

	if acc.Nickname != "" {
		blkop = append(blkop, &gocb.InsertOp{Key: SerializeKey(NicknameLookupType, acc.Nickname), Value: &LookupKey{acc.ID}})
	}

	if err := r.bucket.Do(blkop); err != nil {
		return r.toInternalErr(err)
	}

	for _, op := range blkop {
		insertOp := op.(*gocb.InsertOp)

		if insertOp.Err != nil {
			return r.toInternalErr(insertOp.Err)
		}
	}

	return nil
}

func (r *accountRepository) Update(acc *account.Account) error {
	old, err := r.Get(acc.ID)
	if err != nil {
		return r.toInternalErr(err)
	}

	// TODO: Delete if new email is empty
	if old.Email != acc.Email {
		if err = r.updateReferenceKey(old.Email, acc.Email, acc.ID); err != nil {
			return r.toInternalErr(err)
		}
	}

	if old.Nickname != acc.Nickname {
		if err = r.updateReferenceKey(old.Nickname, acc.Nickname, acc.ID); err != nil {
			return r.toInternalErr(err)
		}
	}

	_, err = r.bucket.Replace(SerializeKey(AccountType, acc.ID.String()), NewDocument(AccountType, 1, acc), 0, 0)
	return r.toInternalErr(err)
}

func (r *accountRepository) updateReferenceKey(old, new string, id uuid.ID) error {
	_, err := r.bucket.Insert(new, &LookupKey{id}, 0)
	if err != nil {
		return r.toInternalErr(err)
	}

	_, err = r.bucket.Remove(old, 0)
	return r.toInternalErr(err)
}

func (r *accountRepository) toInternalErr(err error) error {
	switch err {
	case gocb.ErrKeyNotFound:
		return account.ErrNotFound
	default:
		return err
	}
}
