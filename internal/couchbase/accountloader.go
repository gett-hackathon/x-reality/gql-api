package couchbase

import (
	"time"

	"github.com/couchbase/gocb"
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/account"
)

// NewAccountLoader creates a new couchbase based AccountLoader.
func NewAccountLoader(bucket *gocb.Bucket, maxBatchSize int, maxWait time.Duration) *AccountLoader {
	return &AccountLoader{
		wait:     maxWait,
		maxBatch: maxBatchSize,
		fetch: func(keys []string) ([]*account.Account, []error) {
			blkop := make([]gocb.BulkOp, len(keys))
			errors := make([]error, len(keys))
			result := make([]*account.Account, len(keys))

			for i, key := range keys {
				blkop[i] = &gocb.GetOp{
					Key:   key,
					Value: &Document{Base: &account.Account{}},
				}
			}

			// TODO: Change (Maybe ^^)
			if err := bucket.Do(blkop); err != nil {
				for i := range errors {
					errors[i] = err
				}

				return result, errors
			}

			for i, op := range blkop {
				getOp := op.(*gocb.GetOp)
				if getOp.Err != nil {
					errors[i] = getOp.Err
					break
				}

				result[i] = getOp.Value.(*Document).Base.(*account.Account)
			}

			return result, errors
		},
	}
}
