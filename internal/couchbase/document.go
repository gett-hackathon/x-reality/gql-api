package couchbase

import (
	"strconv"
)

const (
	// CollectionType is the document type of collections
	CollectionType DocumentType = "col"

	// ExtauthType is the document type of extauth accounts
	ExtauthType DocumentType = "extauth"

	// NodeType is the document type of nodes
	NodeType DocumentType = "node"

	// SectionType is the document type name of sections
	SectionType DocumentType = "sec"

	// AccountType is the document type of accounts
	AccountType DocumentType = "acc"

	// EmailLookupType is the document type of email lookup keys
	EmailLookupType DocumentType = "email"

	// NicknameLookupType is the document type of nickname lookup keys
	NicknameLookupType DocumentType = "nick"

	// AppType is the document type of apps
	AppType DocumentType = "app"
)

// DocumentType represents a couchbase document type
type DocumentType string

// MarshalJSON is the custom DocumentType json marshaller
func (t DocumentType) MarshalJSON() ([]byte, error) {
	return []byte(strconv.Quote(string(t))), nil
}

// NewDocument creates a new couchbase document.
func NewDocument(docType DocumentType, version int, base interface{}) *Document {
	return &Document{
		Version: version,
		Type:    docType,
		Base:    base,
	}
}

// Document represents a couchbase document.
type Document struct {
	Base    interface{}  `json:"base"`
	Type    DocumentType `json:"type"`
	Version int          `json:"version"`
}
