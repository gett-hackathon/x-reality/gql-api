package config

import "gitlab.com/gett-hackathon/x-reality/gql-api/internal/errors"

var (
	// ErrInvalidEnvironment occurs when the "env flag" has an invalid value
	ErrInvalidEnvironment = errors.New(errors.Unknown, "invalid environment")
)
