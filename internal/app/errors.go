package app

import (
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/errors"
)

var (
	ErrCategoryNotFound = errors.New(errors.NotFound, "category not found")

	ErrPermissionDenied = errors.New(errors.PermissionDenied, "permission denied")
)
