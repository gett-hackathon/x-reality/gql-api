package app

import (
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/permission"
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/uuid"
)

type permissionedService struct {
	next     Service
	accessor permission.Accessor
}

func NewPermissionedService(accessor permission.Accessor, next Service) Service {
	return &permissionedService{
		accessor: accessor,
		next:     next,
	}
}

func (s *permissionedService) GetAllOwned(accountID uuid.ID) ([]*App, error) {
	return s.next.GetAllOwned(accountID)
}

func (s *permissionedService) Get(id uuid.ID) (*App, error) {
	a, err := s.next.Get(id)
	if err != nil {
		return nil, err
	}

	if !a.CanView(id) {
		return nil, ErrPermissionDenied
	}

	return a, nil
}

func (s *permissionedService) GetAll(accountID uuid.ID) ([]*App, error) {
	if !s.accessor.Is(accountID) {
		return nil, ErrPermissionDenied
	}

	return s.next.GetAll(accountID)
}

func (s *permissionedService) GetSharedApps(accountID uuid.ID) ([]*App, error) {
	if !s.accessor.Is(accountID) {
		return nil, ErrPermissionDenied
	}

	return s.next.GetSharedApps(accountID)
}

func (s *permissionedService) GetByCategory(accountID uuid.ID, category Category) ([]*App, error) {
	if !s.accessor.Is(accountID) {
		return nil, ErrPermissionDenied
	}

	return s.next.GetByCategory(accountID, category)
}

func (s *permissionedService) Create(p Params) (*App, error) {
	if p.OwnerID != nil && (!s.accessor.Is(*p.OwnerID) || s.accessor.Is(permission.UnauthorizedID)) {
		return nil, ErrPermissionDenied
	}

	return s.next.Create(p)
}

func (s *permissionedService) Update(id uuid.ID, p Params) (*App, error) {
	if p.OwnerID != nil && !s.accessor.Is(*p.OwnerID) {
		return nil, ErrPermissionDenied
	}

	return s.next.Update(id, p)
}
