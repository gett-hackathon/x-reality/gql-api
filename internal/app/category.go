package app

import "strings"

type Category int

const (
	Games = iota
	Marketing
	Construction
	Social
	Productivity
	Laboratory
)

var (
	categories = []string{"games", "marketing", "construction", "social", "productivity", "laboratory"}
)

func ToCategory(s string) (Category, error) {
	for i, category := range categories {
		if category == strings.ToLower(s) {
			return Category(i), nil
		}
	}

	return 0, ErrCategoryNotFound
}

func (c Category) String() string {
	return categories[int(c)]
}
