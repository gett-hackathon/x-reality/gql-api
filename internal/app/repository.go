package app

import "gitlab.com/gett-hackathon/x-reality/gql-api/internal/uuid"

type Repository interface {
	Get(id uuid.ID) (*App, error)
	GetAll(accountID uuid.ID) ([]*App, error)
	GetAllOwned(accountId uuid.ID) ([]*App, error)
	GetSharedApps(accountID uuid.ID) ([]*App, error)
	GetByCategory(accountID uuid.ID, category Category) ([]*App, error)

	Update(app *App) error
	Insert(app *App) error
}
