package session

import "gitlab.com/gett-hackathon/x-reality/gql-api/internal/errors"

var (
	// ErrorSessionExpired is returned if the session is expired.
	ErrorSessionExpired = errors.New(errors.SessionExpired, "session expired")

	// ErrorUnexpectedSigningMethod is returned if the token was signed using an unexpected signing method.
	ErrorUnexpectedSigningMethod = errors.New(errors.UnexpectedSigningMethod, "unexpected signing method")

	// ErrorInvalidToken is returned if the token was signed with a wrong key and thus is invalid or has an invalid format.
	ErrorInvalidToken = errors.New(errors.InvalidToken, "invalid token")

	// ErrSecretTooShort occurs when the session secret is too short
	ErrSecretTooShort = errors.New(errors.Unknown, "session secret has to have a length > 50")
)
