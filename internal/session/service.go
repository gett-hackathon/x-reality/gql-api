package session

import (
	"github.com/dgrijalva/jwt-go"
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/config"
)

// Service contains all functionality realted to creating, parsing and verifying sessions.
type Service interface {
	// Token serializes and signs the given jwt claims
	Token(ses jwt.Claims) (string, error)

	// Parse parses the given token into the given claims. The token signature will be checked and the token will be validated using his Valid() method.
	Parse(claims jwt.Claims, token string) error
}

// NewService creates a new session service
func NewService(c config.Config) (Service, error) {
	if len(c.SessionSecret) < 50 {
		return nil, ErrSecretTooShort
	}

	return &service{
		secret: []byte(c.SessionSecret),
	}, nil
}

// service is the basic implementation of Service
type service struct {
	secret []byte
}

// Token serializes and signs the given jwt claims
func (s *service) Token(ses jwt.Claims) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, ses)
	return token.SignedString(s.secret)
}

// Parse parses the given token into the given claims. The token signature will be checked and the token will be validated using his Valid() method.
func (s *service) Parse(claims jwt.Claims, token string) error {
	parsedToken, err := jwt.ParseWithClaims(token, claims, func(parsedToken *jwt.Token) (interface{}, error) {
		if _, ok := parsedToken.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, ErrorUnexpectedSigningMethod
		}

		return s.secret, nil
	})

	if err != nil {
		return err
	}

	if !parsedToken.Valid {
		return ErrorInvalidToken
	}

	return claims.Valid()
}
