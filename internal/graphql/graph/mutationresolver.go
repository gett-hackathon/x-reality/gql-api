package graph

import (
	"context"

	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/account"
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/app"
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/graphql/model"
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/graphql/reqestcontext"
)

type mutationResolver struct{}

// NewMutationResolver creates a new mutation resolver
func NewMutationResolver() MutationResolver {
	return &mutationResolver{}
}

// UpdateAccount is the GraphQL mutation resolver for UpdateAccount
func (r *mutationResolver) UpdateAccount(ctx context.Context, i model.UpdateAccountInput) (*account.Account, error) {
	acr, err := rctx.GetAccessor(ctx)
	if err != nil {
		return nil, err
	}

	srv, err := rctx.GetAccountService(ctx)
	if err != nil {
		return nil, err
	}

	p := account.Params{
		Bio:        i.Bio,
		KnowsAbout: i.KnowsAbout,
		Birthday:   i.Birthday,
		FamilyName: i.FamilyName,
		GivenName:  i.GivenName,
		Nickname:   i.Nickname,
		PictureURL: i.PictureURL,
	}

	if i.Address != nil {
		p.Address = &account.AddressParams{
			Street:  i.Address.Street,
			City:    i.Address.City,
			Country: i.Address.Country,
		}
	}

	return srv.Update(acr.ID, p)
}

func (r *mutationResolver) CreateApp(ctx context.Context, input model.CreateAppInput) (*app.App, error) {
	appSrv, err := rctx.GetAppService(ctx)
	if err != nil {
		return nil, err
	}

	acr, err := rctx.GetAccessor(ctx)
	if err != nil {
		return nil, err
	}

	var category app.Category
	if input.Category != nil {
		category, _ = app.ToCategory(input.Category.String())
	}

	a, err := appSrv.Create(app.Params{
		OwnerID:      &acr.ID,
		Category:     &category,
		Public:       input.Public,
		Contributors: input.Contributors,
		Name:         input.Name,
		Description:  input.Description,
		DownloadURLs: &app.DownloadURLParams{
			Oculus:    input.DownloadUrls.Oculus,
			Vive:      input.DownloadUrls.Vive,
			Microsoft: input.DownloadUrls.Microsoft,
			Google:    input.DownloadUrls.Google,
		},
		LogoURL:     input.LogoURL,
		PreviewURLs: input.PreviewUrls,
		Tags:        input.Tags,
	})

	if err != nil {
		return nil, err
	}

	return a, nil
}

func (r *mutationResolver) UpdateApp(ctx context.Context, input model.UpdateAppInput) (*app.App, error) {
	appSrv, err := rctx.GetAppService(ctx)
	if err != nil {
		return nil, err
	}

	var category *app.Category
	if input.Category != nil {
		*category, _ = app.ToCategory(input.Category.String())
	}

	a, err := appSrv.Update(input.ID, app.Params{
		Category:     category,
		Public:       input.Public,
		Contributors: input.Contributors,
		Name:         input.Name,
		Description:  input.Description,
		DownloadURLs: &app.DownloadURLParams{
			Oculus:    input.DownloadUrls.Oculus,
			Vive:      input.DownloadUrls.Vive,
			Microsoft: input.DownloadUrls.Microsoft,
			Google:    input.DownloadUrls.Google,
		},
		LogoURL:     input.LogoURL,
		PreviewURLs: input.PreviewUrls,
		Tags:        input.Tags,
	})

	if err != nil {
		return nil, err
	}

	return a, nil
}
