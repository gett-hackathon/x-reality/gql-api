package graph

type resolverRoot struct {
	mutation MutationResolver
	query    QueryResolver
	app      AppResolver
}

// NewResolverRoot creates a new resolver root
func NewResolverRoot(mutation MutationResolver, query QueryResolver, app AppResolver) ResolverRoot {
	return &resolverRoot{
		query:    query,
		mutation: mutation,
		app:      app,
	}
}

// Mutation returns the resolver roots mutation resolver
func (r *resolverRoot) Mutation() MutationResolver {
	return r.mutation
}

// Query returns the resolver roots query resolver
func (r *resolverRoot) Query() QueryResolver {
	return r.query
}

func (r *resolverRoot) App() AppResolver {
	return r.app
}
