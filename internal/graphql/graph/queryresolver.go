package graph

import (
	"context"

	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/app"
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/graphql/model"
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/permission"

	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/account"
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/graphql/reqestcontext"
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/uuid"
)

type queryResolver struct{}

// NewQueryResolver creates a new query resolver
func NewQueryResolver() QueryResolver {
	return &queryResolver{}
}

func (r *queryResolver) AllApps(ctx context.Context) ([]app.App, error) {
	acr, err := rctx.GetAccessor(ctx)
	if err != nil {
		return nil, err
	}

	srv, err := rctx.GetAppService(ctx)
	if err != nil {
		return nil, err
	}

	apps, err := srv.GetAll(acr.ID)
	if err != nil {
		return nil, err
	}

	res := make([]app.App, len(apps))
	for i, a := range apps {
		res[i] = *a
	}

	return res, nil
}

func (r *queryResolver) AllOwnedApps(ctx context.Context) ([]app.App, error) {
	acr, err := rctx.GetAccessor(ctx)
	if err != nil {
		return nil, err
	}

	srv, err := rctx.GetAppService(ctx)
	if err != nil {
		return nil, err
	}

	apps, err := srv.GetAllOwned(acr.ID)
	if err != nil {
		return nil, err
	}

	res := make([]app.App, len(apps))
	for i, a := range apps {
		res[i] = *a
	}

	return res, nil
}

func (r *queryResolver) AppsByCategory(ctx context.Context, category model.Category) ([]app.App, error) {
	acr, err := rctx.GetAccessor(ctx)
	if err != nil {
		return nil, err
	}

	srv, err := rctx.GetAppService(ctx)
	if err != nil {
		return nil, err
	}

	c, err := app.ToCategory(category.String())
	if err != nil {
		return nil, err
	}

	apps, err := srv.GetByCategory(acr.ID, c)
	if err != nil {
		return nil, err
	}

	res := make([]app.App, len(apps))
	for i, a := range apps {
		res[i] = *a
	}

	return res, nil
}

func (r *queryResolver) SharedApps(ctx context.Context) ([]app.App, error) {
	acr, err := rctx.GetAccessor(ctx)
	if err != nil {
		return nil, err
	}

	srv, err := rctx.GetAppService(ctx)
	if err != nil {
		return nil, err
	}

	apps, err := srv.GetSharedApps(acr.ID)
	if err != nil {
		return nil, err
	}

	res := make([]app.App, len(apps))
	for i, a := range apps {
		res[i] = *a
	}

	return res, nil
}

// Account is the GraphQL query resolver for Account
func (r *queryResolver) Account(ctx context.Context, id uuid.ID) (*account.Account, error) {
	srv, err := rctx.GetAccountService(ctx)
	if err != nil {
		return nil, err
	}

	return srv.Get(id)
}

// AccountByNickname is the GraphQL query resolver for AccountByNickname
func (r *queryResolver) AccountByNickname(ctx context.Context, nickname string) (*account.Account, error) {
	srv, err := rctx.GetAccountService(ctx)
	if err != nil {
		return nil, err
	}

	return srv.GetByNickname(nickname)
}

func (r *queryResolver) App(ctx context.Context, id uuid.ID) (*app.App, error) {
	srv, err := rctx.GetAppService(ctx)
	if err != nil {
		return nil, err
	}

	return srv.Get(id)
}

// Viewer is the GraphQL query resolver for Viewer
func (r *queryResolver) Viewer(ctx context.Context) (*account.Account, error) {
	acr, err := rctx.GetAccessor(ctx)
	if err != nil {
		return nil, err
	}

	if acr.Is(permission.UnauthorizedID) {
		return nil, nil
	}

	srv, err := rctx.GetAccountService(ctx)
	if err != nil {
		return nil, err
	}

	return srv.Get(acr.ID)
}
