package graphql

import (
	"context"
	"github.com/99designs/gqlgen/graphql"
	"github.com/vektah/gqlparser/gqlerror"
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/errors"
)

// NewErrorPresenter creates new GraphQL error presenter which masks error messages with generic error codes and messages provided by the internal error package
func NewErrorPresenter() graphql.ErrorPresenterFunc {
	return func(ctx context.Context, err error) *gqlerror.Error {
		if err, ok := err.(*gqlerror.Error); ok {
			err.Path = graphql.GetResolverContext(ctx).Path()
			return err
		}

		code := errors.Unknown
		if e, ok := err.(*errors.Error); ok {
			code = e.Code()
		}

		return &gqlerror.Error{
			Message:    code.String(),
			Path:       graphql.GetResolverContext(ctx).Path(),
			Extensions: map[string]interface{}{"code": code},
		}
	}
}
