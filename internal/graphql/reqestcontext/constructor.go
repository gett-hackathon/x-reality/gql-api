package rctx

import (
	"context"
	"time"

	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/app"
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/config"
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/logging"
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/session"

	"github.com/couchbase/gocb"
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/couchbase"
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/permission"

	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/account"
)

// ContextKey represents a requestContext context key.
type ContextKey string

const (
	// AccessorContextKey is used to store and retrieve the accessor in a QGL request context.
	AccessorContextKey = ContextKey("Accessor")

	// AccountServiceContextKey is used to store and retrieve the account service in a QGL request context.
	AccountServiceContextKey = ContextKey("AccountService")

	AppServiceContextKey = ContextKey("AppService")
)

// NewInjector creates a new Injector
func NewInjector(bucket *gocb.Bucket, log logging.Logger, sessionSrv session.Service, appService app.Service, c config.Config) *Injector {
	return &Injector{
		bucket:     bucket,
		log:        log,
		sessionSrv: sessionSrv,
		config:     c,
		appService: appService,
	}
}

// Injector is used to inject internal services into a GQL request context
type Injector struct {
	bucket     *gocb.Bucket
	log        logging.Logger
	config     config.Config
	sessionSrv session.Service
	appService app.Service
}

// Inject injects internal services and the accessor information into a GQL request context
func (i *Injector) Inject(ctx context.Context, accessor permission.Accessor) context.Context {
	dlAccountRepo := couchbase.NewAccountRepository(i.bucket, 500, time.Millisecond)

	accountService := account.NewService(dlAccountRepo, i.sessionSrv, i.config)

	accountService = account.NewPermissionedService(accountService, accessor)
	appService := app.NewPermissionedService(accessor, i.appService)

	log := i.log.WithFields("accessor", accessor)
	accountService = account.NewLoggingService(accountService, log)

	ctx = context.WithValue(ctx, AccessorContextKey, accessor)
	ctx = context.WithValue(ctx, AccountServiceContextKey, accountService)
	ctx = context.WithValue(ctx, AppServiceContextKey, appService)

	return ctx
}

// GetAccessor returns the in the request context injected accessor
func GetAccessor(ctx context.Context) (permission.Accessor, error) {
	accessor, ok := ctx.Value(AccessorContextKey).(permission.Accessor)
	if !ok {
		return permission.Accessor{}, ErrNotInContext
	}

	return accessor, nil
}

// GetAccountService returns the in the request context injected account service.
func GetAccountService(ctx context.Context) (account.Service, error) {
	service, ok := ctx.Value(AccountServiceContextKey).(account.Service)
	if !ok {
		return nil, ErrNotInContext
	}

	return service, nil
}

func GetAppService(ctx context.Context) (app.Service, error) {
	service, ok := ctx.Value(AppServiceContextKey).(app.Service)
	if !ok {
		return nil, ErrNotInContext
	}

	return service, nil
}
