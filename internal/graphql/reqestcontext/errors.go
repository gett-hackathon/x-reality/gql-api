package rctx

import "gitlab.com/gett-hackathon/x-reality/gql-api/internal/errors"

var (
	// ErrNotInContext occurs if a requested value isn't in the request context
	ErrNotInContext = errors.New(errors.NotFound, "value not in request context")
)
