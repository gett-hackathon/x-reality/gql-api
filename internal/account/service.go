package account

import (
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/config"
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/session"
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/uuid"
)

// Service provides all the directly to accounts related functionality.
type Service interface {
	// Create creates a new account using the given params.
	Create(p Params, isEmailVerified bool) (*Account, error)

	// Get returns the account with the given ID.
	Get(id uuid.ID) (*Account, error)

	// GetByNickname returns an account with the given nickname
	GetByNickname(nick string) (*Account, error)

	// GetByEmail returns an account with the given email
	GetByEmail(email string) (*Account, error)

	// GetPublic returns an account with all for other users visible information.
	GetPublic(id uuid.ID) (*Account, error)

	// Update updates the account with the given ID using the provided params.
	Update(id uuid.ID, p Params) (*Account, error)

	// VerifyEmail verifies an accounts email using the given token.
	VerifyEmail(token string) (*Account, error)
}

// NewService creates a new account service
func NewService(repo Repository, sessionSrv session.Service, c config.Config) Service {
	return &service{
		repo:                 repo,
		sessionSrv:           sessionSrv,
		emailVerificationURL: c.EmailActivationURL,
	}
}

type service struct {
	repo                 Repository
	sessionSrv           session.Service
	emailVerificationURL string
}

// Create creates a new account using the given params. The params will be validated.
// Email, nickname will be checked for duplicate entries and won't be accepted if there are any.
func (s *service) Create(p Params, isEmailVerified bool) (*Account, error) {
	acc, err := New(p, isEmailVerified)
	if err != nil {
		return nil, err
	}

	_, err = s.repo.GetByEmail(acc.Email)
	if err != nil && err != ErrNotFound {
		return nil, err
	}

	if err == nil {
		return nil, ErrEmailTaken
	}

	_, err = s.repo.GetByNickname(acc.Nickname)
	if err != nil && err != ErrNotFound {
		return nil, err
	}

	if err == nil {
		return nil, ErrNicknameTaken
	}

	return acc, s.repo.Insert(acc)
}

// Get returns the account with the given ID.
func (s *service) Get(id uuid.ID) (*Account, error) {
	return s.repo.Get(id)
}

// GetByNickname returns an account with the given nickname.
func (s *service) GetByNickname(nick string) (*Account, error) {
	return s.repo.GetByNickname(nick)
}

// GetByEmail returns an account with the given email.
func (s *service) GetByEmail(email string) (*Account, error) {
	return s.repo.GetByEmail(email)
}

// GetPublic returns the account with the given id. Only public fields will be visible.
func (s *service) GetPublic(id uuid.ID) (*Account, error) {
	acc, err := s.Get(id)
	if err != nil {
		return nil, err
	}

	return acc.PublicInfo(), nil
}

// Update updates the account with the given id using the params. Only non-nil fields will be updated.
func (s *service) Update(id uuid.ID, p Params) (*Account, error) {
	acc, err := s.Get(id)
	if err != nil {
		return nil, err
	}

	if err := acc.Update(p); err != nil {
		return nil, err
	}

	return acc, s.repo.Update(acc)
}

// VerifyEmail verifies an accounts email using the given token.
func (s *service) VerifyEmail(token string) (*Account, error) {
	sess := &session.EmailVerification{}
	err := s.sessionSrv.Parse(sess, token)
	if err != nil {
		return nil, err
	}

	id, err := uuid.FromString(sess.Subject)
	if err != nil {
		return nil, err
	}

	acc, err := s.repo.Get(id)
	if err != nil {
		return nil, err
	}

	if acc.Email != sess.Email {
		return nil, ErrEmailMismatch
	}

	if acc.IsEmailVerified {
		return nil, ErrEmailAlreadyVerified
	}

	acc.IsEmailVerified = true
	return acc, s.repo.Update(acc)
}
