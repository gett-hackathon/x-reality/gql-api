package account

import (
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/uuid"
)

// Repository provides the basic database CRUD operations used by Service.
type Repository interface {
	// Get returns the account with the given id.
	Get(id uuid.ID) (*Account, error)

	// Get returns the account with the given nickname.
	GetByNickname(nickname string) (*Account, error)

	// Get returns the account with the given email.
	GetByEmail(email string) (*Account, error)

	// Insert inserts the given account into the database.
	Insert(acc *Account) error

	// Update updates the given account in the database. The account will be identified by its id.
	Update(acc *Account) error
}
