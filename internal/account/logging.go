package account

import (
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/logging"
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/uuid"
)

// loggingService provides a logging wrapper around Service.
type loggingService struct {
	next Service
	log  logging.Logger
}

// NewLoggingService creates a new logging service.
func NewLoggingService(next Service, log logging.Logger) Service {
	return &loggingService{
		log:  log.WithFields("service", "account"),
		next: next,
	}
}

// Create provides a logging wrapper around service.Create.
func (s *loggingService) Create(p Params, isEmailVerified bool) (acc *Account, err error) {
	defer s.log.Debugw("called method Create", "params", p, "isEmailVerified", isEmailVerified, "account", acc, "error", err)
	return s.next.Create(p, isEmailVerified)
}

// Get provides a logging wrapper around service.Get.
func (s *loggingService) Get(id uuid.ID) (acc *Account, err error) {
	defer s.log.Debugw("called method Get", "id", id, "account", acc, "error", err)
	return s.next.Get(id)
}

// GetByEmail provides a logging wrapper around service.GetByEmail.
func (s *loggingService) GetByEmail(email string) (acc *Account, err error) {
	defer s.log.Debugw("called method GetByEmail", "email", email, "account", acc, "error", err)
	return s.next.GetByEmail(email)
}

// GetByNickname provides a logging wrapper around service.GetByNickname.
func (s *loggingService) GetByNickname(nick string) (acc *Account, err error) {
	defer s.log.Debugw("called method GetByNickname", "nickname", nick, "account", acc, "error", err)
	return s.next.GetByEmail(nick)
}

// GetPublic provides a logging wrapper around service.GetPublic.
func (s *loggingService) GetPublic(id uuid.ID) (acc *Account, err error) {
	defer s.log.Debugw("called method GetPublic", "id", id, "account", acc, "error", err)
	return s.next.GetPublic(id)
}

// Update provides a logging wrapper around service.Update.
func (s *loggingService) Update(id uuid.ID, p Params) (acc *Account, err error) {
	defer s.log.Debugw("called method Update", "id", id, "params", p, "account", acc, "error", err)
	return s.next.Update(id, p)
}

// VerifyEmail provides a logging wrapper around service.VerifyEmail.
func (s *loggingService) VerifyEmail(token string) (acc *Account, err error) {
	defer s.log.Debugw("called method VerifyEmail", "token", token, "account", acc, "error", err)
	return s.next.VerifyEmail(token)
}
