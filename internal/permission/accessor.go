package permission

import "gitlab.com/gett-hackathon/x-reality/gql-api/internal/uuid"

// UnauthorizedID is the id of an unauthorized accessor.
const UnauthorizedID = uuid.ID("unauthorized")

// Accessor represents a barbra accessor.
type Accessor struct {
	ID uuid.ID
}

// NewAccessor creates a new accessor
func NewAccessor(accountID uuid.ID) Accessor {
	return Accessor{accountID}
}

// Is returns if the accessor belongs to the account with the given ID.
func (a Accessor) Is(id uuid.ID) bool {
	return a.ID == id
}

// InUnauthorized returns true if the accessor is unauthorized.
func (a Accessor) InUnauthorized() bool {
	return a.ID == UnauthorizedID
}
