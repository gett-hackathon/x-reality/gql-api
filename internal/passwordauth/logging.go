package passwordauth

import (
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/logging"
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/uuid"
)

// loggingService provides a logging wrapper around Service.
type loggingService struct {
	next Service
	log  logging.Logger
}

// NewLoggingService creates a new logging service.
func NewLoggingService(next Service, log logging.Logger) Service {
	return &loggingService{
		log:  log.WithFields("service", "PasswordAuth"),
		next: next,
	}
}

// Register provides a logging wrapper around service.Register.
func (s *loggingService) Register(p RegisterParams) (id uuid.ID, err error) {
	defer s.log.Debugw("called method Register", "params", p, "id", id, "error", err)
	return s.next.Register(p)
}

// Register provides a logging wrapper around service.Register.
func (s *loggingService) Login(p LoginParams) (id uuid.ID, err error) {
	defer s.log.Debugw("called method Login", "params", p, "id", id, "error", err)
	return s.next.Login(p)
}
