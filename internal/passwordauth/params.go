package passwordauth

import (
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/account"
	"gitlab.com/barbra-group/validate"
)

// RegisterParams are the parameters allowed during registration.
type RegisterParams struct {
	Password   string `json:"password"`
	Nickname   string `json:"nickname"`
	Email      string `json:"email"`
	GivenName  string `json:"given_name"`
	FamilyName string `json:"family_name"`
	PictureURL string `json:"picture_url"`
}

// LoginParams are the parameters allowed during registration
type LoginParams struct {
	Password string
	Login    string
}

// AccountParams returns an account params object which includes everything set in the RegisterParams
func (p RegisterParams) AccountParams() account.Params {
	return account.Params{
		Nickname:   &p.Nickname,
		Email:      &p.Email,
		GivenName:  &p.GivenName,
		FamilyName: &p.FamilyName,
		PictureURL: &p.PictureURL,
	}
}

// Validate validates the RegisterParams (only the password because the rest will be checked during the account creation)
func (p RegisterParams) Validate() error {
	return validate.Fields("registration params",
		validate.String(p.Password, "password", false, validate.HasRuneLength(7, 150)),
	)
}

// Validate validates the LoginParams (only the password because the rest will be checked by the account service)
func (p LoginParams) Validate() error {
	return validate.Fields("registration params",
		validate.String(p.Password, "password", false, validate.HasRuneLength(7, 150)),
	)
}
