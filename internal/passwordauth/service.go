package passwordauth

import (
	"gitlab.com/barbra-group/validate"
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/account"
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/uuid"
)

// Service provides the logic necessary to provide password based login/registration.
type Service interface {
	// Register registers a new account using the given RegisterParams.
	// If the login is successful the accounts id is returned.
	Register(p RegisterParams) (uuid.ID, error)

	// Login logs a user in using his login (email or password) and his password.
	// If the login is successful the accounts id is returned.
	Login(p LoginParams) (uuid.ID, error)
}

type service struct {
	accSrv account.Service
	repo   Repository
}

// NewService creates a new password auth service
func NewService(repo Repository, accSrv account.Service) Service {
	return &service{
		repo:   repo,
		accSrv: accSrv,
	}
}

// Register registers a new account using the given RegisterParams.
// If the login is successful the accounts id is returned.
func (s *service) Register(p RegisterParams) (uuid.ID, error) {
	if err := p.Validate(); err != nil {
		return "", err
	}

	acc, err := s.accSrv.Create(p.AccountParams(), false)
	if err != nil {
		return "", err
	}

	i, err := NewInfo(acc.ID, p.Password)
	if err != nil {
		return "", err
	}

	if err := s.repo.InsertInfo(i); err != nil {
		return "", err
	}

	return acc.ID, nil
}

// Login logs a user in using his login (email or password) and his password.
// If the login is successful the accounts id is returned.
func (s *service) Login(p LoginParams) (uuid.ID, error) {
	if err := p.Validate(); err != nil {
		return "", err
	}

	// Check if the login is an email address or nickname and set getFunc accordingly
	var getFunc func(string) (*account.Account, error)
	if err := validate.IsEmail.Validate(p.Login); err != nil {
		getFunc = s.accSrv.GetByEmail
	} else {
		getFunc = s.accSrv.GetByNickname
	}

	acc, err := getFunc(p.Login)
	if err != nil {
		return "", err
	}

	i, err := s.repo.GetInfo(acc.ID)
	if err != nil {
		return "", err
	}

	if err := i.Verify(p.Password); err != nil {
		return "", err
	}

	return acc.ID, nil
}
