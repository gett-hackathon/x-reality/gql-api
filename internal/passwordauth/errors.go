package passwordauth

import "gitlab.com/gett-hackathon/x-reality/gql-api/internal/errors"

var (
	// ErrPasswordNotSet is returned if the account password was not set.
	// This is most likely because the user registered with an OpenID account and hasn't set his password yet.
	ErrPasswordNotSet = errors.New(errors.PasswordNotSet, "password not set")

	// ErrWrongPassword occurs when the entered password is invalid
	ErrWrongPassword = errors.New(errors.PermissionDenied, "wrong password")

	// ErrPasswordTooShort is returned if a new password is created using a string of less then 7 chars.
	ErrPasswordTooShort = errors.New(errors.PasswordToShort, "password is too short")

	// ErrNotFound is returned if the password auth account wasn't found
	ErrNotFound = errors.New(errors.NotFound, "password auth account not found")
)
